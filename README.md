				______________PSB-MATLAB INTERFACE README_____________ 
				______________________version 1.0_____________________
	
_________A. 	Introduction 

The purpose of this program is to wrap pandapower functionalities regarding 
1. Network element creation
2. Network diagnostics 
3. Network plotting and result visualization
4. Power flow algorithms
5. Network optimization
and place them in a more general context that extends the above capabilities  through interfacing
with other programming languages such as MATLAB. This extension allows for the application of 
optimization and control modules not normally present in pandapower (or Python).

1. 	The program attempts to follow the 'logic' of pandapower as closely as possible, namely by consolidating 
	all network creation, KPI & state calculation and variable mapping operations in one object (net_handle.py).
	This object is then passed around to the other modules for the application of optimization (net_optimization.py)
	and result handling (postprocessing_utilities.py) operations.

2.	The program can, currently, apply optimal power flow operations for a network snapshot, as well as control operations
	for a network timeseries. NOTE: The control operations on timeseries is static, i.e. it is just sequential evaluations 
	of network snapshots, which is identical to what the pandapower timeseries module does. Future extensions should accommodate
	for dynamic control operations (pseudo-real time) that can factor in for the elapsed time of each optimization time step, 
	as well as 'noise', for a more realistic assessment of the solvers.

________B. 	Interfacing with solvers 

1.	The program, currently, is oriented to use MATLAB-based solvers (colloquially referred to as 'algorithms' or 'optimizers'). 
	Solvers exist in the 'solvers/' directory, and	are known to the program by the child folder name, for example, '/PSO' 
	(the child folder bears the name of the algorithm). The 'optimizer.m' performs the optimization and returns the results to
	the Python module, therefore it should always exist in a solvers' directory, regardless of other files.

2.	A 'solver' optimizes the 'objective value' of an 'objective function'. In the MATLAB environment, an optimizer must
	repeatedly call an objective_function.m function throughout the optimization process. For single-objective solvers
	the aforementioned function must yield a single scalar value (the 'objective value'). Other objective functions can
	be created and placed in the '/obj_functions' directory, customized with regards to state constraint handling,
	multiobjectivity, etc. The program, currently, accepts as an input the name of the objective function to be used. In
	future extensions, this should occur automatically (since it is an obscure technicality for the user).

3. 	The objective function accepts a vector with the design variables of the optimization (hereby referred to as 'design vector'),
	and returns the objective value that corresponds to them.