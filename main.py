"""
This file wraps all processes of grid simulation (create, simulate, report)
:author: Myron Papadimitrakis (TELSIP, University of West Attica)
:date: 11/09/2020
"""

from postprocessing_utilities import PostProcessor
from net_optimization import NetOptimization
from net_handle import NetHandle
import os
import pathlib
import yaml

class TelsipPSB:
    """
    This class interfaces between HTTP server and the Telsip PSB utilities like
    net creation, optimization and result handling.
    """
    def __init__(self, post_data, matlab_engine):
        self.post_data = post_data
        self.standard_net_directory = TelsipPSB._load_pdpower_net_directory(self)
        self.solver_catalog = TelsipPSB._update_algorithm_catalog(self)
        self.matlab_engine = matlab_engine

    def return_catalog(self):
        return self.solver_catalog

    def run_opf(self):
        if not TelsipPSB._algorithm_exists(self):
            return 'algorithm nonexistent'
        architecture = self.post_data['architecture']
        optimization_config = self.post_data['optimization_config']
        snapshot_vector = self.post_data['snapshot_vector']

        # Create topology
        net_handle = TelsipPSB._create_topology(self, architecture)
        opf_results = TelsipPSB._run_objective_opf(self, net_handle, optimization_config,
                                                   snapshot_vector)
        return opf_results

    def run_control(self):
        if not TelsipPSB._algorithm_exists(self):
            return 'algorithm nonexistent'
        architecture = self.post_data['architecture']
        optimization_config = self.post_data['optimization_config']
        timeseries = self.post_data['timeseries']

        # Create topology
        net_handle = TelsipPSB._create_topology(self, architecture)
        control_results = TelsipPSB._run_objective_control(self, net_handle, optimization_config,
                                                           timeseries)
        return control_results

    def _algorithm_exists(self):
        optim_conf = self.post_data['optimization_config']
        solver = optim_conf['solver_config'].get('solver')
        if solver in self.solver_catalog['solvers']:
            return True
        else:
            return False

    def _load_pdpower_net_directory(self):
        mf = pathlib.Path('pandapower_networks_directory.yaml')
        with open(mf) as fp:
            yaml_dict = yaml.safe_load(fp)
        network_directory = yaml_dict
        return network_directory

    def _create_topology(self, architecture):
        net_handle = NetHandle()
        msg = net_handle.create_net_from_architecture(architecture)
        print(msg)
        return net_handle

    def _load_default_topology(self, case_name):
        net_handle = NetHandle()
        msg = net_handle.create_net_from_pandapower_library(case_name, self.standard_net_directory)
        print(msg)
        return net_handle

    def _run_objective_opf(self, net_handle, optimization_config, network_snapshot):
        net_optimization = NetOptimization(net_handle, optimization_config.get('power_flow_config'),
                                           matlab_engine=self.matlab_engine)
        print(net_optimization.run_opf_matlab(optimization_config, network_snapshot))
        results = TelsipPSB._handle_results(self, net_optimization)
        return results

    def _run_objective_control(self, net_handle, optimization_config, network_timeseries):
        net_optimization = NetOptimization(net_handle, optimization_config.get('power_flow_config'),
                                           matlab_engine=self.matlab_engine)
        print(net_optimization.run_control_matlab(optimization_config, network_timeseries))

    def _update_algorithm_catalog(self):

        # Scan directories for changes. WARNING: ignores directories starting with __ (2 underscores)
        solvers = os.listdir('./solvers')
        # Remove solvers from list that don't contain the optimizer.m file
        item_number = 0
        solvers_valid = []
        for item in solvers:
            optimizer_path = os.path.abspath('./solvers/{!s}/optimizer.m'.format(item))
            optimizer_exists = os.path.isfile(optimizer_path)
            if optimizer_exists:
                solvers_valid.append(solvers[item_number])
            item_number += 1
        solvers = solvers_valid
        solver_defaults_all = {}
        for solver_id in solvers:
            # The following block grabs all solver defaults and concatenates them in the same dictionary
            model_config_path = "solvers/{!s}/solver_defaults.yaml".format(solver_id)
            mf = pathlib.Path(model_config_path)
            with open(mf) as fp:
                model_info_dict = yaml.safe_load(open(mf))
            solver_defaults_all[solver_id] = model_info_dict[solver_id]
        # Remove .m suffix
        solver_info = {'solvers': solvers, 'solver_defaults_all': solver_defaults_all}
        return solver_info

    def _handle_results(self, net_handle):
        postprocessor = PostProcessor(net_handle)
        return 'test'

if __name__ == "__main__":
    import yaml
    import pathlib
    import pandas as pd

    server_url = 'localhost:8081'
    mf = pathlib.Path('architecture2.yaml')
    with open(mf) as fp:
        yaml_dict = yaml.safe_load(fp)
    architecture = yaml_dict

    mf = pathlib.Path('optimization_config.yaml')
    with open(mf) as fp:
        yaml_dict = yaml.safe_load(fp)
    optimization_config = yaml_dict

    network_snapshot = pd.read_csv('dummy_snapshot.csv')
    network_snapshot = network_snapshot.squeeze()
    post_data = {'architecture': architecture, 'optimization_config': optimization_config,
                 'snapshot_vector': network_snapshot}
    telsip_psb = TelsipPSB( post_data, {})
    telsip_psb.run_opf()