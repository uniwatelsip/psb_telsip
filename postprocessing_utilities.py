import pandapower as pdpower
import pandas as pd
import pandapower.plotting
from pandapower.plotting import cmap_discrete, create_line_trace, draw_traces

# TODO: @Nikos, lets finish this. This class should be invoked in the context of main.py, and
#       it should accept a net_handle object. Net_handle contains the pandapower net object along with
#       other parameters and structures. Read net_handle.py code carefully and lets disucss this.


class PostProcessor:
    """
    This Class extracts the results from the net_handle object and reforms them in a presentable
    way. It accepts a net_handle object and extracts all relevant data. The PostProcessor is able
    to:
    - Plot the network
    - Supply information regarding state variables for a given network state (bus voltages, line
      loadings in % and kA)
    - Verify that there are no violated state constraints for a given network state (line loadings,
      bus voltages, external grid)
    """

    def __init__(self, net_handle):
        self.net_handle = net_handle

    @staticmethod
    def visualize_network(net, configuration):
        """
        Visualizes the network
        """
        # Simple network plot
        pdpower.plotting.simple_plot(net, respect_switches=False, line_width=1.0, bus_size=1.0, ext_grid_size=1.0,
                                    trafo_size=1.0, plot_loads=False, plot_sgens=False, load_size=1.0,
                                    sgen_size=1.0, switch_size=2.0, switch_distance=1.0, plot_line_switches=False,
                                    scale_size=True, bus_color='b', line_color='grey', trafo_color='k',
                                    ext_grid_color='y', switch_color='k', library='igraph', show_plot=True, ax=None)


        # pdpower.plotting.plotly.simple_plotly(net, respect_switches=True, use_line_geodata=None, on_map=False,
        #                                       projection=None, map_style='basic', figsize=1, aspectratio='auto',
        #                                       line_width=1, bus_size=10, ext_grid_size=20.0, bus_color='blue',
        #                                       line_color='grey', trafo_color='green', ext_grid_color='yellow')

        # Plot a network colored and labeled according to voltage levels
        # pandapower.plotting.plotly.vlevel_plotly(net, respect_switches=True, use_line_geodata=None, colors_dict=None,
        #                                          on_map=False, projection=None, map_style='basic', figsize=1,
        #                                          aspectratio='auto', line_width=2, bus_size=10)

        # Plot a network according to power flow results
        pdpower.plotting.plotly.pf_res_plotly(net, cmap='Jet', use_line_geodata=None, on_map=None, projection='epsg:31467',
                                              map_style='dark', figsize=1, aspectratio='auto', line_width=2, bus_size=10,
                                              filename='temp-plot.html')

    def save_network(self,net):
        """"
        Saves the network
        """
        pp.to_pickle(net, "example_net.p")

    def pf_results_network(self,net, sidx, max_load, min_vm_pu, max_vm_pu):
        """"
        Prints several information about the network
        """
        switches_res = pdpower.switch_info(net, sidx) #
        load_res = pdpower.lf_info(net)   # info about max/min voltage, max trafo load, max line load
        lines_res = pdpower.overloaded_lines(net, max_load) # lines with loading_percent > max_load
        buses_res = pdpower.violated_buses(net, min_vm_pu, max_vm_pu)  # bus indices where vm_pu is not within min_vm_pu
                                                                       # and max_vm_pu

        return switches_res, load_res, lines_res, buses_res

    def diagnostic_pf_report(self, net):
        """"
        Returns a dictionary with logging of the most common issues leading to errors, eg negative element indices,
        lines with impedance zero, elements that are disconnected from the network
        """
        diag_results = pdpower.diagnostic(net, report_style='detailed', warnings_only=False, return_result_dict=True,
                              overload_scaling_factor=0.001, min_r_ohm=0.001, min_x_ohm=0.001, min_r_pu=1e-05,
                              min_x_pu=1e-05, nom_voltage_tolerance=0.3, numba_tolerance=1e-05)

        return diag_results

    def opf_results_network(self,net):
        opf_res = pdpower.opf_task(net)  # prints basic info says the documentation :P

        return opf_res

