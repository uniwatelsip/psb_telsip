"""
:author: Myron Papadimitrakis (University of West Attica)
:date: 11/09/2020
"""

import requests
import time
import json
import pathlib
import yaml
import pandas as pd

def run_optimization(url, architecture, optimization_config, snapshot_vector):
    """
    This method packs all relevant data in a post request, and requests an optimization run. The
    server sequentially does the following
    1. Creates a net object based on architecture, applies the snapshot vector
    2. Runs an optimization based on optimization_config
    3. Returns the setpoints, along with some critical network info
    :param url: str (telsipbackend.hopto.org:8081)
    :param architecture: dict, see architecture.yaml and brainstorming pdf
    :param optimization_config: dict, see optimization_config.yaml and brainstorming pdf
    :param snapshot_vector: pd.Series, see snapshot_vector.csv
    :return: dict {'setpoints': setpoint dict, 'state_variables': state_variable_dict,
                   'info': info_dict}
    """
    # Serialize passed parameters in order to be inserted in post request

    post_data = {'architecture': architecture, 'optimization_config': optimization_config,
                 'snapshot_vector': snapshot_vector.to_dict(orient='list')}
    url = "http://{!s}/run_optimization".format(url)
    start_time = time.time()
    r = requests.post(url, json=post_data)
    # TODO: Manage response string
    response_string = r.text
    try:
        result_dict = json.loads(response_string)
        print("Results: {!s}".format(result_dict))
    except:
        # Must be an error message
        print(response_string)
    end_time = time.time()
    print("Optimization completed. Time elapsed:", end_time - start_time)


if __name__ == "__main__":

    server_url = 'localhost:8081'
    mf = pathlib.Path('architecture2.yaml')
    with open(mf) as fp:
        yaml_dict = yaml.safe_load(fp)
    architecture = yaml_dict

    mf = pathlib.Path('optimization_config.yaml')
    with open(mf) as fp:
        yaml_dict = yaml.safe_load(fp)
    optimization_config = yaml_dict

    network_snapshot = pd.read_csv('dummy_snapshot.csv')

    run_optimization(server_url, architecture, optimization_config, network_snapshot)