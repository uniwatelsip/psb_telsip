function [optimal_obj_value, optimal_design_vector] = optimizer(connection_config, solver_config,...
                                    bounds_upper_struct, bounds_lower_struct, states_max_struct,...
                                    states_min_struct, kpi_dict, obj_fun_str)

    % Extract parameters and form them into matlab arrays
    [states_max, states_min, bounds_upper, bounds_lower, ...
     kpi_weight, no_design_vars, no_constrained_states] = arrayfy_parameters(bounds_upper_struct, ...
                                           bounds_lower_struct, states_max_struct, states_min_struct, ...
                                           kpi_dict);
    % Extract connection configuration parameters, establish connection
    % (Do make sure that all numerical connection parameters passed are of type double because
    % MATLAB is retar... unique)
    if strcmp(connection_config.host, '')
        host = 'localhost';
    else
        host = connection_config.host;
    end
    connection = tcpip(host, double(connection_config.port));
    connection.OutputBufferSize = double(connection_config.buffer_size);
    connection.InputBufferSize = double(connection_config.buffer_size);
    fopen(connection);

    % Display information:
    disp(strcat('Connection to  ', host, ' established'))
    disp(strcat('Optimizing network with ', num2str(no_design_vars),' design variables and  ', ...
         num2str(no_constrained_states),' constrained network states.'))

    % Set options and objective function handle.
    obj_fun_handle = str2func(obj_fun_str);
    options = optimoptions('particleswarm','MaxTime',double(solver_config.max_time),'Display','iter',...
                            'SwarmSize',double(solver_config.swarm_size), ...
                            'MaxStallIterations',double(solver_config.stall_iterations));
    % Execute dummy optimization
    obj_func=@(design_vector) obj_fun_handle(connection, kpi_weight, states_max, states_min, ...
                                             design_vector);
    design_vector = bounds_lower;
    obj_value = obj_func(design_vector);
    optimal_obj_value = 0;
    optimal_design_vector = 0;
    fclose(connection);
end

function [states_max, states_min, bounds_upper, bounds_lower, ...
          kpi_weight, no_design_vars, no_constrained_states] = arrayfy_parameters(bounds_upper_struct, ...
                                           bounds_lower_struct, ...
                                           states_max_struct, states_min_struct, ...
                                           kpi_dict)

    % Extract bound constraints from bounds structure, form them into an array.
    design_var_names = fieldnames(bounds_upper_struct);
    no_design_vars = numel(design_var_names);
    for bound_idx = 1:no_design_vars
        design_var_name = string(design_var_names(bound_idx));
        bounds_upper(bound_idx) = double(getfield(bounds_upper_struct, design_var_name));
        bounds_lower(bound_idx) = double(getfield(bounds_lower_struct, design_var_name));
    end

    % Extract state constraints from constraints structure, form them into an array.
    constrained_state_names = fieldnames(states_max_struct);
    no_constrained_states = numel(constrained_state_names);
    for constraint_idx = 1:no_constrained_states
        state_name = string(constrained_state_names(constraint_idx));
        states_max(constraint_idx) = double(getfield(states_max_struct, state_name));
        states_min(constraint_idx) = double(getfield(states_min_struct, state_name));
    end

    % Extract KPI weights from KPI structure, form them into an array.
    kpi_names = fieldnames(kpi_dict);
    no_kpis = numel(kpi_names);
    for kpi_idx = 1:no_kpis
        kpi_name = string(kpi_names(kpi_idx));
        kpi_weight(kpi_idx) = double(getfield(kpi_dict, kpi_name));
    end
end