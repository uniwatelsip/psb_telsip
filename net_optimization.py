


import pandapower as pdpower
import os
import threading
import time
import numpy as np

class NetOptimization:
    """
    This class consolidates all possible Net optimization features. NetOptimization methods can be
    evaluated repeatedly for the same NetHandle object, and the results will be stored in a list
    """
    def __init__(self, NetHandle, power_flow_config, matlab_engine):
        self.net_handle = NetHandle
        self.matlab_engine = matlab_engine
        self.matlab_engine.addpath('obj_functions')
        self.results_structure = []
        self.power_flow_config = power_flow_config

    def run_power_flow(self):
        pf_conf = self.power_flow_config
        pdpower.runpp(self.net_handle.return_net(), algorithm=pf_conf['algo'],
                      calculate_voltage_angles='auto', init='auto', max_iteration='auto',
                      tolerance_mva=1e-08, trafo_model='t', trafo_loading='current',
                      enforce_q_lims=pf_conf['enforce_q_lims'], check_connectivity=True,
                      voltage_depend_loads=True, run_control=pf_conf['run_control'], numba=False)
        return 'Power flow calcs completed'

    def run_opf(self, optimization_config):
        opf_conf = optimization_config
        results = pdpower.runopp(self.net_handle['net'], verbose=False, calculate_voltage_angles=False,
                                 check_connectivity=True, suppress_warnings=True, switch_rx_ratio=2,
                                 delta=1e-10, init='flat', numba=True, trafo3w_losses='hv',
                                 consider_line_temperature=False)

    def run_opf_matlab(self, optimization_config, network_snapshot):
        from tcp_utilities import TCPServer

        # Extract configs and parameters
        self.optimization_config = optimization_config
        kpi_config = self.optimization_config.get('kpi_config')
        kpi_weights = kpi_config.get('kpi_weights')
        solver_config = self.optimization_config.get('solver_config')
        state_config = self.optimization_config.get('state_config')
        server_config = self.optimization_config.get('server_config')
        power_flow_handle = NetOptimization.run_power_flow
        # store optimization configs and grab serialized bounds and constraints
        self.net_handle.store_optimization_configs(kpi_config, state_config)
        constraints_dict, bounds_dict, serialized_keys, design_variable_types = \
            self.net_handle.return_serialized_bounds_constraints()
        solver_path = 'solvers/{!s}'.format(solver_config['solver'])

        # Add solver directory so that optimizer.m is discoverable
        matlabeng = self.matlab_engine
        abs_solver_path = os.path.abspath(solver_path)
        matlabeng.addpath(abs_solver_path)

        # Apply network snapshot (if None, that means it is already loaded on net, so skip it)
        if network_snapshot is not None:
            snapshot_datetime = network_snapshot.pop('datetime')
            network_snapshot = network_snapshot.squeeze()  # (squeeze it to Series)
            self.net_handle.apply_snapshot_vector(network_snapshot)

        # Initialize TCP-IP service thread
        time_start = time.time()
        tcp_server = TCPServer(server_config, power_flow_handle=power_flow_handle,
                               optimization_self=self)
        thread = threading.Thread(target=NetOptimization._threaded_server_start, args=(self, tcp_server))
        thread.start()

        # Execute optimization
        obj_fun_str = solver_config.get('obj_fun')
        obj_value, output_vector = matlabeng.optimizer(server_config, solver_config,
                                                       bounds_dict['upper'], bounds_dict['lower'],
                                                       constraints_dict['max'], constraints_dict['min'],
                                                       design_variable_types, kpi_weights,
                                                       obj_fun_str, nargout=2)
        [output_vector] = np.array(output_vector).tolist()
        # Create result dict, append to result structure
        elapsed_time = time_start - time.time()
        metadata = {'elapsed_time': elapsed_time, 'obj_value': obj_value}
        setpoints = self.net_handle.label_raw_design_vector(output_vector)
        NetOptimization._append_to_results_structure(self, metadata, setpoints)

        # Remove path and join thread. The TCP server closes automatically after one client has
        # been served.
        matlabeng.rmpath(abs_solver_path)
        thread.join()

        return 'Matlab optimization completed'

    def run_control_matlab(self, optimization_config, network_timeseries):
        from tcp_utilities import TCPServer

        # Extract configs and parameters
        self.optimization_config = optimization_config
        kpi_config = self.optimization_config.get('kpi_config')
        kpi_weights = kpi_config.get('kpi_weights')
        solver_config = self.optimization_config.get('solver_config')
        state_config = self.optimization_config.get('state_config')
        server_config = self.optimization_config.get('server_config')
        power_flow_handle = NetOptimization.run_power_flow

        # store optimization configs and grab serialized bounds and constraints
        self.net_handle.store_optimization_configs(kpi_config, state_config)
        constraints_dict, bounds_dict, serialized_keys, design_variable_types = \
            self.net_handle.return_serialized_bounds_constraints()
        solver_path = 'solvers/{!s}'.format(solver_config['solver'])

        # Add solver directory so that optimizer.m is discoverable
        matlabeng = self.matlab_engine
        abs_solver_path = os.path.abspath(solver_path)
        matlabeng.addpath(abs_solver_path)

        # Iterate through network_timeseries
        obj_value_list = []
        output_vector_list = []
        elapsed_time_list = []
        datetime_list = []
        for index, network_snapshot in network_timeseries.iterrows():
            time_start = time.time()
            print(network_snapshot)
            snapshot_datetime = network_snapshot.pop('datetime')
            self.net_handle.apply_snapshot_vector(network_snapshot)
            tcp_server = TCPServer(server_config, power_flow_handle=power_flow_handle,
                                   optimization_self=self)
            thread = threading.Thread(target=NetOptimization._threaded_server_start, args=(self, tcp_server))
            thread.start()

            # Execute optimization
            obj_fun_str = solver_config.get('obj_fun')
            obj_value, output_vector = matlabeng.optimizer(server_config, solver_config,
                                                           bounds_dict['upper'], bounds_dict['lower'],
                                                           constraints_dict['max'], constraints_dict['min'],
                                                           design_variable_types, kpi_weights,
                                                           obj_fun_str, nargout=2)
            [output_vector] = np.array(output_vector).tolist()
            elapsed_time = time_start - time.time()

            obj_value_list.append(obj_value)
            output_vector_list.append(self.net_handle.label_raw_design_vector(output_vector))
            elapsed_time_list.append(elapsed_time)
            datetime_list.append(snapshot_datetime)

            # Join thread. The TCP server closes automatically after one client has been served.
            thread.join()
        # Create result dict, append to result structure

        metadata = {'elapsed_time': elapsed_time_list, 'obj_value': obj_value_list}
        setpoints = output_vector_list
        NetOptimization._append_to_results_structure(self, metadata, setpoints, datetime_list)

        # Remove path and join thread. The TCP server closes automatically after one client has
        # been served.
        matlabeng.rmpath(abs_solver_path)

        return 'Matlab optimization completed'

    def _append_to_results_structure(self, metadata, setpoints):
        """
        This method appends to the results list
        :param metadata: dict {could be anything regarding the optimization process}
        :param setpoints: dict {'ELEMENT_1': {'parameter_name': design_var1},
                                'ELEMENT_2': {'parameter_name': design_var2,...}, ...}
        """
        result_dict = {'config': self.optimization_config, 'metadata': metadata,
                       'setpoints': setpoints}
        self.results_structure.append(result_dict)

    def return_results_structure(self):
        return self.results_structure

    def _threaded_server_start(self, tcp_server):
        tcp_server.launch_tcp_server()