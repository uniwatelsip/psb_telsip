% DEMO PYTHON-MATLAB TCP-IP SOCKET INTERFACE
% This creates a vector in matlab, passes it to python via asynchronous TCP-IP, and
% python returns it with each element doubled. This client utilizes a UUID per TCP request.
clc
clear all
% 
% Start server
!python tcp_server_uuid.py &

% Create connection and set input and output buffer size
connection = tcpip('localhost', 50007);
buffer_size_bytes = 10000;
connection.OutputBufferSize = buffer_size_bytes;
connection.InputBufferSize = buffer_size_bytes;

% Create a vector
design_vector = randn(1,50)*10000;
design_vector = round(design_vector, 6);
% Open connection and use TCP interface to pass the vector. Close the connection
fopen(connection);
tic;
response = tcpip_interface(connection, design_vector);
response_time = toc;
fclose(connection);

% Check response
disp(strcat('Total response time: ', num2str(response_time)))
check_response = response == design_vector*2;  % Is the response the expected one?
if all(check_response)
    disp('All elements are equal')
else
    nonzero_count = length(check_response) - sum(check_response);
    disp(strcat('Response has ', num2str(nonzero_count),' non-equal elements'))
end