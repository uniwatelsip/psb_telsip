# DEMO PYTHON-MATLAB TCP-IP SOCKET INTERFACE
# This server accepts a vector from matlab via TCP-IP, parses it and returns it with each element doubled.
# This server utilizes a UUID per TCP request.

import ast
import numpy as np
import math
import time
from tcp_uuid_socket import Socket

HOST = ''
PORT = 50007
s = Socket()
s.bind((HOST, PORT))
s.listen(100)

while True:
    c, a = s.accept()
    print(f'server: {a[0]}:{a[1]} connected')
    while True:
        data_msg = c.get_msg()
        if not data_msg: break

        data_msg = data_msg.replace(' ', ', ')
        X = ast.literal_eval(data_msg)

        A = 10
        obj_value = A*len(X) + sum([(x**2 - A * np.cos(2 * math.pi * x)) for x in X])

        c.put_msg(obj_value)
    print(f'server: {a[0]}:{a[1]} disconnected')
    c.close()
