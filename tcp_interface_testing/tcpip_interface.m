% TCPIP Interface that utilizes UUID per packet.

function message = tcpip_interface(connection, design_vector)
    java_uuid =  java.util.UUID.randomUUID;
    uuid_request = char(java_uuid.toString);
    design_vector_str = mat2str(design_vector);
    msg = strcat(uuid_request, design_vector_str);
    tcp_message = [msg newline];
    fwrite(connection, tcp_message);

    % Response read loop. Handle UUID
    valid_uuid_found = false;
    empty_responses = 0;
    empty_responses_threshold = 100;
    empty_resp_threshold_reached = false;
    wrong_uuid = 0;
    wrong_uuid_threshold = 30;   % This should be the number of total requests that the caller will use
    while not(valid_uuid_found)

        response = fread(connection, [1, connection.BytesAvailable]);
        response_str = native2unicode(response, 'UTF-8');

        % Use a try-catch here because the response could be empty, thus
        % breaking on response_str(1:36)
        try
            uuid_response = response_str(1:36);
            % check that the returned UUID is the same as the expected one
            if strcmp(uuid_response, uuid_request)
                valid_uuid_found = true;
            else
                wrong_uuid = wrong_uuid+1;
                % check that we havent reached max wrong UUIDs
                if wrong_uuid > wrong_uuid_threshold
                    ME = MException('TCPInterface: ThresholdUUID', 'Reached threshold number of wrong UUIDs')
                    throw(ME)
                end
            end
        catch
            empty_responses = empty_responses+1;
            % check that we havent reached max empty responses
            if empty_responses > empty_responses_threshold
                ME = MException('TCPInterface: ThresholdEmptyResponses', 'Reached threshold number of empty responses')
                throw(ME)
            end
        end
    end
    % Parse the message
    message_str = response_str(37:end);
    message_str = erase(message_str, ',');
    message = str2num(message_str);

end
% --------------------------
% CONSIDERATIONS:
% --------------------------
% 1)One should not expect that the significant digit precision to be maintained
%   when data is passed through the interface. Consider this vector:
%   [1.51111111515151151e+25 2.113333333338 3.51222222222222228 4.1222222222282 -0.1];
%   Observe that we have elements with variable degrees of SD precision. If
%   used as a design vector, the elements with the high SD precision in the
%   response will not be *exactly* the same as the corresponding vector in MATLAB.
%   Therefore, one should take care not to mix high SD precision elements
%   with low SD ones when using the interface. In practical terms, this means
%   rounding to 7-8 decimals or less.
% 2) There is a maximum number of design variables that can be utilized for a given
%    buffer size. Make sure that this is correctly set.

