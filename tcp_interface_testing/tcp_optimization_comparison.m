% ======== TCP-IP INTERFACE VERIFICATION ==========
% This script applies the MATLAB PSO solver for the optimization of the Rastrigin function.
% The purpose of this script is to verify the integrity of the TCP-IP interface,
% when used in an optimization framework. The objective function is calculated in Matlab and in Python, and
% the optimization problem is solved multiple times for different number of design variables. The results are
% averaged out in order to contain stochasticity and are compared in terms of performance, objective value and total time elapsed.

%
clc
clear all
% 
% Start server
!python tcp_server_rastrigin.py &

% Set python server Connection parameters
connection = tcpip('localhost', 50007);
buffer_size_bytes = 10000;
connection.OutputBufferSize = buffer_size_bytes;
connection.InputBufferSize = buffer_size_bytes;

% Set optimization options
options = optimoptions('particleswarm','MaxTime',3600*8,'Display','iter',...
                        'SwarmSize',20,'MaxStallIterations',10);

% Set maximum number of design variables to iterate through
max_no_design_vars = 10;
% Set total iterations per number of design variables
max_iters = 2;

variables_sae_matlab = zeros(max_iters,max_no_design_vars);
variables_sae_python = zeros(max_iters,max_no_design_vars);
matlab_fval = zeros(max_iters,max_no_design_vars);
python_fval = zeros(max_iters,max_no_design_vars);
matlab_time = zeros(max_iters,max_no_design_vars);
python_time = zeros(max_iters,max_no_design_vars);
matlab_iters = zeros(max_iters,max_no_design_vars);
python_iters = zeros(max_iters,max_no_design_vars);
matlab_fcount = zeros(max_iters,max_no_design_vars);
python_fcount = zeros(max_iters,max_no_design_vars);


for iter = 1:max_iters
    for no_design_vars=2:max_no_design_vars
        lower_bounds = -5.12*ones(1,no_design_vars);
        upper_bounds = 5.12*ones(1,no_design_vars);

        % Run matlab PSO
        rng(5);
        obj_fun_matlab=@(x) rastrigin_matlab(x);
        tic
        [matlab_optimum,matlab_fval(iter, no_design_vars),exitflag, matlab_output] = particleswarm(obj_fun_matlab, ...
                                         no_design_vars,lower_bounds,upper_bounds,options);
        matlab_time(iter, no_design_vars) = toc;
        matlab_iters(iter, no_design_vars) = matlab_output.iterations;
        matlab_fcount(iter, no_design_vars) = matlab_output.funccount;

        % Run python PSO
        fopen(connection);
        rng(5);
        obj_fun_python=@(x) rastrigin_python(connection, x);
        tic
        [python_optimum,python_fval(iter, no_design_vars),exitflag, python_output] = particleswarm(obj_fun_python, ...
                                         no_design_vars,lower_bounds,upper_bounds,options);
        python_time(iter, no_design_vars) = toc;
        python_iters(iter, no_design_vars) = python_output.iterations;
        python_fcount(iter, no_design_vars) = python_output.funccount;
        fclose(connection);

        % Metrics
        variables_sae_matlab(iter, no_design_vars) = sumabs(matlab_optimum);
        variables_sae_python(iter, no_design_vars) = sumabs(python_optimum);
    end
end

% Calculate averages
variables_sae_matlab_avg = mean(variables_sae_matlab, 1);
variables_sae_python_avg = mean(variables_sae_python, 1);
matlab_time_avg = mean(matlab_time, 1);
python_time_avg = mean(python_time, 1);
python_fval_avg = mean(python_fval, 1);
matlab_fval_avg = mean(matlab_fval, 1);
matlab_iters_avg = mean(matlab_iters, 1);
python_iters_avg = mean(python_iters, 1);
matlab_fcount_avg = mean(matlab_fcount, 1);
python_fcount_avg = mean(python_fcount, 1);

% Plot all the malakies
comparison_plots(variables_sae_matlab_avg, variables_sae_python_avg, matlab_time_avg, ...
                          python_time_avg, matlab_fval_avg, python_fval_avg, matlab_iters_avg, ...
                          python_iters_avg, matlab_fcount_avg, python_fcount_avg)


% Objective functions:
function obj_value_matlab = rastrigin_matlab(x)
    rastrigin_time = tic;
    n = size(x, 2);
    A = 10;
    obj_value_matlab = (A * n) + (sum(x .^2 - A * cos(2 * pi * x), 2));
    disp(strcat('Matlab OF time: ', num2str(toc(rastrigin_time))))
end

function obj_value_python = rastrigin_python(connection, x)
    rastrigin_time = tic;
    obj_value_python = tcpip_interface(connection, x);
    disp(strcat('Python OF time: ', num2str(toc(rastrigin_time))))
end

function comparison_plots(variables_sae_matlab_avg, variables_sae_python_avg, matlab_time_avg, ...
                          python_time_avg, matlab_fval_avg, python_fval_avg, matlab_iters_avg, ...
                          python_iters_avg, matlab_fcount_avg, python_fcount_avg)

    f1 = figure;
    p = uipanel('Parent',f1,'BorderType','none');
    p.Title = 'Performance';
    p.TitlePosition = 'centertop';
    p.FontSize = 12;
    p.FontWeight = 'bold';

    subplot(2,1,1,'Parent',p)
    plot(variables_sae_matlab_avg)
    grid minor
    hold on
    plot(variables_sae_python_avg)
    legend('Matlab', 'TCP-IP Python')
    title('Sum of absolute errors of design variables from origin')
    xlabel('Number of design variables')
    ylabel('SAE')

    subplot(2,1,2,'Parent',p)
    plot(matlab_fval_avg)
    grid minor
    hold on
    plot(python_fval_avg)
    legend('Matlab', 'TCP-IP Python')
    title('Objective value')
    xlabel('Number of design variables')
    ylabel('Objective value')

    f2 = figure;
    p = uipanel('Parent',f2,'BorderType','none');
    p.Title = 'Evaluation';
    p.TitlePosition = 'centertop';
    p.FontSize = 12;
    p.FontWeight = 'bold';

    subplot(3,1,1,'Parent',p)
    plot(matlab_iters_avg)
    grid minor
    hold on
    plot(python_iters_avg)
    legend('Matlab', 'TCP-IP Python')
    title('PSO iterations until convergence')
    xlabel('Number of design variables')

    subplot(3,1,2,'Parent',p)
    plot(matlab_fcount_avg)
    grid minor
    hold on
    plot(python_fcount_avg)
    legend('Matlab', 'TCP-IP Python')
    title('Obj. function evaluations until convergence')
    xlabel('Number of design variables')

    subplot(3,1,3,'Parent',p)
    plot(matlab_time_avg)
    grid minor
    hold on
    plot(python_time_avg)
    legend('Matlab', 'TCP-IP Python')
    title('Time to convergence')
    xlabel('Number of design variables')
    ylabel('Seconds')
end