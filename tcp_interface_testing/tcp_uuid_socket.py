# TCP-IP Socket
from socket import *

class Socket:
    def __init__(self,s=None):
        '''default create a new socket, or wrap an existing one.
        '''
        self.sock = socket() if s is None else s
        self.buffer = b''
        self.uuid = ''
        self.total_packets = 0

    def connect(self,addr):
        self.sock.connect(addr)
        self.client_address = addr

    def bind(self, addr):
        self.client_address = addr
        self.sock.bind(addr)

    def listen(self,n):
        self.sock.listen(n)

    def accept(self):
        c, a = self.sock.accept()
        # Wrap the client socket in a Socket.
        return Socket(c),a

    def get_msg(self):
        # Buffer data until a newline is found.
        while b'\n' not in self.buffer:
            data = self.sock.recv(10000)
            if not data:
                return b''
            self.buffer += data
        # split off the message bytes from the buffer.
        msg,_,self.buffer = self.buffer.partition(b'\n')
        msg_string = msg.decode('utf-8')
        self.uuid = msg_string[:36]
        actual_msg = msg_string[36:]
        self.total_packets = self.total_packets + 1
        actual_msg = actual_msg.replace(' ', ', ')
        return actual_msg

    def put_msg(self, msg):
        # Add UUID
        uuid_msg = self.uuid + str(msg)
        response = f'{uuid_msg}'
        self.sock.sendall(response.encode('utf-8') + b'\n')

    def put_multiple_msg(self, msg_list):
        delimiter = '/'
        concat_msg = str(msg_list.pop(0))
        for msg in msg_list:
            concat_msg = concat_msg + delimiter + str(msg)
        uuid_msg = self.uuid + concat_msg
        response = f'{uuid_msg}'
        self.sock.sendall(response.encode('utf-8') + b'\n')

    def close(self):
        print('Total packets received: {!s}'.format(self.total_packets))
        self.sock.close()


if __name__ == '__main__':
    Socket()
