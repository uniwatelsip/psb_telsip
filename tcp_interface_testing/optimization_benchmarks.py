# Optimization benchmarks to be used for tcp_optimization_comparison

import numpy as np
import math


def rastrigin(X):
    A = 10
    rastrigin_value = A + sum([(x**2 - A * np.cos(2 * math.pi * x)) for x in X])
    return rastrigin_value

