# Python TCP IP Server. Doubles the vector that matlab sends over.

import socket
import ast
import numpy as np
HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print('Connected by {!s}'.format(addr))
while True:
    data = conn.recv(10000)
    data_str = data.decode("utf-8")
    if len(data_str) > 2:
        data_str = data_str.replace('b', '')
        data_str = data_str.replace(' ', ', ')
        data_list = ast.literal_eval(data_str)
        data_list = [i*2 for i in data_list]
        response_str = str(data_list)
        response_buffer = response_str.encode('utf-8')
    if not data: break
    conn.sendall(response_buffer)
conn.close()


