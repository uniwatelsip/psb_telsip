% DEMO PYTHON-MATLAB TCP-IP SOCKET INTERFACE
% This creates a vector in matlab, passes it to python via TCP-IP, and
% python returns it with each element doubled.
clc
clear all
% 
% Start server
!python tcp_server_uuid_multiple.py &

% Create connection and set input and output buffer size
connection = tcpip('localhost', 50007);
buffer_size_bytes = 10000;
connection.OutputBufferSize = buffer_size_bytes;
connection.InputBufferSize = buffer_size_bytes;
total_length_to_test = 200;
round_to_decimal = 6;
response_time_pervector = zeros(1,total_length_to_test);
nonequal_elements_pervector = zeros(1,total_length_to_test);
fopen(connection);
for vector_length= 2:100
    % Create a vector
    design_vector = randn(1,vector_length)*10000;
    design_vector = round(design_vector, round_to_decimal);
    % Open connection and use TCP interface to pass the vector. Close the connection
    tic;
    response = tcpip_interface(connection, design_vector);
    response_time = toc;
    % Is the response the expected one?
    response_vec = response{1};
    check_response = response_vec == design_vector*2;
    if all(check_response)
        disp('All elements are equal')
        nonzero_count = 0;
    else
        nonzero_count = length(check_response) - sum(check_response);
        disp(strcat('Response has ', num2str(nonzero_count),' non-equal elements'))
    end
    response_time_pervector(vector_length) = response_time;
    nonequal_elements_pervector(vector_length) = nonzero_count;
    pause(0.1)
end
fclose(connection);
plot(response_time_pervector);
hold on
xlabel('number of elements in vector')
ylabel('overhead (s)')