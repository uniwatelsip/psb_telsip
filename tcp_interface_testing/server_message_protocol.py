
from tcp_interface_testing.tcp_uuid_socket import Socket
import ast

HOST = ''
PORT = 50006
s = Socket()
s.bind((HOST, PORT))
s.listen(10)

while True:
    c, a = s.accept()
    print(f'server: {a[0]}:{a[1]} connected')
    while True:
        data_msg = c.get_msg()
        print(data_msg)
        if not data_msg: break
        data_msg = data_msg.replace(' ', ', ')
        data_list = ast.literal_eval(data_msg)
        data_list = [i*2 for i in data_list]
        response_str = str(data_list)
        c.put_msg(f'[{response_str}]')
    print(f'server: {a[0]}:{a[1]} disconnected')
    c.close()
