# DEMO PYTHON-MATLAB TCP-IP SOCKET INTERFACE
# This server accepts a vector from matlab via TCP-IP, parses it and returns it with each element doubled.
# This server utilizes a UUID per TCP request.

from tcp_uuid_socket import Socket
import ast

HOST = ''
PORT = 50007
s = Socket()
s.bind((HOST, PORT))
s.listen(100)

while True:
    c, a = s.accept()
    print(f'server: {a[0]}:{a[1]} connected')
    while True:
        data_msg = c.get_msg()
        if not data_msg: break
        data_list = ast.literal_eval(data_msg)
        data_list = [i*2 for i in data_list]
        c.put_msg(data_list)
    print(f'server: {a[0]}:{a[1]} disconnected')
    c.close()
