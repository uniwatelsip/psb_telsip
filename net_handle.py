import pandapower as pdpower
import pandas as pd
import numpy as np
import re


class NetHandle:
    """
    This class wraps all features of a pandapower net, plus additional functionalities
    dictated by the TCP-IP interface.

    __NOMENCLATURE__
    network element: Refers to an element entity such as transformer,
    design_vector: User-specified vector that updates parameters of controllable elements in the
                   network. A design_vector contains design_variables, that refer to the parameters
                   of the controllable element. NOTE: a controllable element could have more than
                   one controllable parameters (ie. injected active + reactive power of an sgen)
    element_name (sometimes element_id): Refers to the EMTech (global) name (BUS_1, SGEN_5 etc)
    pdpower indices: Each element in an element type group has its own local numeric index in the
                     pandapower context. For example, BUS_1 and SGEN_1 both have the same local
                     numeric index (they belong to different groups).
    net_mapping: Contains nested dictionaries for each element type group (line, bus, sgens etc)
                 that describe the ID mapping between element_name and local pdpower indices.
    serialized_key: A serialized key is a name that points to a specific element parameter of
                    interest. This element parameter could be for example a design variable
                    in an optimization context. Its importance pertains to sanity-checking
                    the order of the passed parameters through TCP-IP
    Bound constraint: Limits for each network design variable.
    State constraint: Limits for each network state variable.
    kpis: FOR THE SCOPE OF THIS CLASS, 'kpis' refer to technical KPIs upon which the optimization
          is based. Voltage_deviation is an actionable KPI (that is, it can be fed into an
          optimization solver)
    states: FOR THE SCOPE of this class, 'states' refer to network operational variables that
            are not user defined. Example of state variables are line loadings and bus voltages.
            All state names follow pandapower nomenclature
            (see https://github.com/e2nIEE/pandapower/blob/master/tutorials/powerflow.ipynb )

    """

    def __init__(self):

        # Create empty network
        self.net = pdpower.create_empty_network()

        # Create net_mapping dictonary between global (EMTech) element names and local (pandapower) numeric indices.
        # !Warning!: The net_mapping dictionary should be used within context, because even though the EMTech
        # names are global, the pdpower names are not - pandapower resets the numeric index for each element
        # type (bus, line etc). Therefore, do not attempt to iterate through the keys if you are not 100% sure that
        # they point to the intended element type.
        self.net_mapping = {}

        # Create state variable mapping dictionary between pandapower and MATLAB solver. The MATLAB solver parameters
        # such as upper/lower bounds and state constraints are abstracted - this means that only min and max are req'd.
        self.state_constraints = {}

        # Create two cost mapping dictionaries - one for piecewise linear, and one for polynomial cost functions.
        self.cost_coefs = {}

        # Nomenclature mapping
        self.pdpower_element_types = {'BUS': 'bus', 'LINE': 'line', 'SW': 'switch', 'LOAD': 'load',
                                      'SGEN': 'sgen', 'EXT': 'ext_grid', 'GEN': 'gen',
                                      'TRAFO': 'trafo', 'SHUNT': 'shunt'}

    def create_net_from_architecture(self, net_architecture):
        """
        This method creates a pandapower net object from the architecture YAML file, and stores it
        in self. As the architecture file is parsed, a mapping dictionary is created between EMTech network
        nomenclature and pandapower element indexing. Also, design variable bounds and
        state variable constraints are extracted and stored separately for easier handling & passing
        to the MATLAB solver
        """
        self.net_architecture = net_architecture
        architecture = self.net_architecture['Architecture']
        self.controllable_elements = self.net_architecture.get('Controllable_Elements')
        self.controllable_element_type = {}
        # Get all element lists
        self.buses = architecture.get('Bus')
        self.lines = architecture.get('Line')
        self.switches = architecture.get('Switch')
        self.loads = architecture.get('Load')
        self.sgens = architecture.get('Static_Generator')
        self.ext_grid = architecture.get('External_Grid')
        self.trafos = architecture.get('Transformer')
        self.generators = architecture.get('Generator')
        self.shunts = architecture.get('Shunt')
        self.storage = architecture.get('Storage')
        self.cost_functions = self.net_architecture.get('Cost_Functions')

        # Create all elements, category by category. The order is important and should be left as shown.
        # Each try/except block parses a specific element category.

        # Parse buses
        try:
            # check if dict is empty
            if bool(self.buses):
                for bus_config in self.buses:
                    # Create bus
                    element_id = bus_config['name']
                    self.net_mapping[element_id] = pdpower.create_bus(self.net, name=element_id,
                                                                      type=bus_config['type'],
                                                                      vn_kv=bus_config['vn_kv'],
                                                                      max_vm_pu=bus_config['max_vm_pu'],
                                                                      min_vm_pu=bus_config['min_vm_pu'])
                    # Store state constraints
                    self.state_constraints[element_id] = \
                        {'bus_voltage': {'max': bus_config['max_vm_pu'], 'min': bus_config['min_vm_pu'],
                                         'neutral': 1}}
        except KeyError:
            return 'One or more keys were not found while parsing buses'
        except RuntimeError:
            return 'Unknown runtime error while parsing buses'

        # Parse transformers
        try:
            # check if dict is empty
            if bool(self.trafos):
                for trafo_config in self.trafos:
                    element_id = trafo_config['name']
                    hv_bus = self.net_mapping[trafo_config['hv_bus']]
                    lv_bus = self.net_mapping[trafo_config['lv_bus']]
                    self.net_mapping[element_id] = \
                        pdpower.create_transformer_from_parameters(self.net,
                                                                   name=element_id,
                                                                   hv_bus=hv_bus,
                                                                   lv_bus=lv_bus,
                                                                   sn_mva=trafo_config[
                                                                       'sn_mva'],
                                                                   vn_hv_kv=trafo_config[
                                                                       'vn_hv_kv'],
                                                                   vn_lv_kv=trafo_config[
                                                                       'vn_lv_kv'],
                                                                   vkr_percent=trafo_config[
                                                                       'vkr_percent'],
                                                                   vk_percent=trafo_config[
                                                                       'vk_percent'],
                                                                   pfe_kw=trafo_config[
                                                                       'pfe_kw'],
                                                                   i0_percent=trafo_config[
                                                                       'i0_percent'],
                                                                   tap_max=trafo_config[
                                                                       'tap_max'],
                                                                   tap_min=trafo_config[
                                                                       'tap_min'],
                                                                   tap_neutral=trafo_config[
                                                                       'tap_neutral'],
                                                                   max_loading_percent=
                                                                   trafo_config[
                                                                       'max_loading_percent']
                                                                   )
                    # Store state constraints
                    self.state_constraints[element_id] = \
                        {'trafo_loading': {'max': trafo_config['max_loading_percent'],
                                           'min': np.nan, 'neutral': 1},
                         'tap': {'max': trafo_config['tap_max'], 'min': trafo_config['tap_min'],
                                 'neutral': trafo_config['tap_neutral']}
                         }
        except KeyError:
            return 'One or more keys were not found while parsing transformers'
        except RuntimeError:
            return 'Unknown runtime error while parsing transformers'

        # Parse external grid
        try:
            # check if dict is empty
            if bool(self.ext_grid):
                for extgrid_config in self.ext_grid:
                    element_id = extgrid_config['name']
                    extgrid_bus = self.net_mapping[extgrid_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_ext_grid(self.net, name=element_id,
                                                                           bus=extgrid_bus, vm_pu=0, va_degree=0,
                                                                           max_p_mw=extgrid_config['max_p_mw'],
                                                                           min_p_mw=extgrid_config['min_p_mw'],
                                                                           max_q_mvar=extgrid_config['max_q_mvar'],
                                                                           min_q_mvar=extgrid_config['min_q_mvar'])
                    # Store state constraints
                    self.state_constraints[element_id] = \
                        {'extgrid_MW': {'max': extgrid_config['max_p_mw'], 'min': extgrid_config['min_p_mw'],
                                        'neutral': 1},
                         'extgrid_MVar': {'max': extgrid_config['max_q_mvar'], 'min': extgrid_config['min_q_mvar'],
                                          'neutral': 1}}
                    # Apply element parameters
                    parameter_names_to_apply = ['vm_pu', 'va_degree']
                    parameters_to_apply = dict((k, extgrid_config[k]) for k in parameter_names_to_apply
                                               if k in extgrid_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing external grid'
        except RuntimeError:
            return 'Unknown runtime error while parsing external grid'

        # Parse lines
        try:
            # check if dict is empty
            if bool(self.lines):
                for line_config in self.lines:
                    element_id = line_config['name']
                    bus_from = self.net_mapping[line_config['from']]  # Get local indices from bus mapping
                    bus_to = self.net_mapping[line_config['to']]
                    self.net_mapping[element_id] = pdpower.create_line(self.net, name=element_id,
                                                                       from_bus=bus_from, to_bus=bus_to,
                                                                       std_type=line_config['std_type'],
                                                                       length_km=line_config['length_km'],
                                                                       max_loading_percent=line_config[
                                                                           'max_loading_percent'])
                    # Store state constraints
                    self.state_constraints[element_id] = \
                        {'line_loading': {'max': line_config['max_loading_percent'],
                                          'min': 0, 'neutral': np.nan}}
        except KeyError:
            return 'One or more keys were not found while parsing lines'
        except RuntimeError:
            return 'Unknown runtime error while parsing lines'

        # Parse switches
        try:
            # check if dict is empty
            if bool(self.switches):
                for switch_config in self.switches:
                    element_id = switch_config['name']
                    bus_from = self.net_mapping[switch_config['from']]  # Get local indices from bus mapping
                    bus_to = self.net_mapping[switch_config['to']]
                    self.net_mapping[element_id] = pdpower.create_switch(self.net, name=element_id,
                                                                         bus=bus_from, element=bus_to, et='b')

                    if element_id in self.controllable_elements:  # check if element is cont/ble
                        controllable_element_dict = self.controllable_elements[element_id]
                        controllable_element_dict['closed']['upper'] = 1
                        controllable_element_dict['closed']['lower'] = 0

                        #  Put controllable element dict back in central dict
                        self.controllable_elements[element_id] = controllable_element_dict

                    # Apply element parameters
                    parameter_names_to_apply = ['closed']
                    parameters_to_apply = dict((k, switch_config[k]) for k in parameter_names_to_apply
                                               if k in switch_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing switches'
        except RuntimeError:
            return 'Unknown runtime error while parsing switches'

        # Parse loads
        try:
            # check if dict is empty
            if bool(self.loads):
                for load_config in self.loads:
                    element_id = load_config['name']
                    on_bus = self.net_mapping[load_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_load(self.net,
                                                                       name=element_id, bus=on_bus, p_mw=0,
                                                                       q_mvar=0,
                                                                       max_p_mw=load_config['max_p_mw'],
                                                                       min_p_mw=load_config['min_p_mw'],
                                                                       max_q_mvar=load_config['max_q_mvar'],
                                                                       min_q_mvar=load_config['min_q_mvar'])
                    # Apply element parameters
                    parameter_names_to_apply = ['p_mw', 'q_mvar']
                    parameters_to_apply = dict((k, load_config[k]) for k in parameter_names_to_apply
                                               if k in load_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing loads'
        except RuntimeError:
            return 'Unknown runtime error while parsing loads'

        # Parse static generators
        try:
            # check if dict is empty
            if bool(self.sgens):
                for sgen_config in self.sgens:
                    element_id = sgen_config['name']
                    on_bus = self.net_mapping[sgen_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_sgen(self.net, name=element_id,
                                                                       bus=on_bus, p_mw=0, q_mvar=0,
                                                                       max_p_mw=sgen_config['max_p_mw'],
                                                                       min_p_mw=sgen_config['min_p_mw'],
                                                                       max_q_mvar=sgen_config['max_q_mvar'],
                                                                       min_q_mvar=sgen_config['min_q_mvar']
                                                                       )
                    # Grab design variable bounds - Note: a controllable element could have multiple
                    # design variables.
                    if element_id in self.controllable_elements:  # check if element is cont/ble
                        controllable_element_dict = self.controllable_elements[element_id]
                        # Iterate through each design variable in the controllable element, grab bounds
                        for variable_name in controllable_element_dict:
                            if variable_name == 'q_mvar':
                                controllable_element_dict[variable_name]['upper'] = \
                                    sgen_config['max_q_mvar']
                                controllable_element_dict[variable_name]['lower'] = \
                                    sgen_config['min_q_mvar']
                            elif variable_name == 'p_mw':
                                controllable_element_dict[variable_name]['upper'] = \
                                    sgen_config['max_p_mw']
                                controllable_element_dict[variable_name]['lower'] = \
                                    sgen_config['min_p_mw']
                        #  Put controllable element dict back in central dict
                        self.controllable_elements[element_id] = controllable_element_dict

                    # Apply element parameters
                    parameter_names_to_apply = ['scaling', 'p_mw', 'q_mvar']
                    parameters_to_apply = dict((k, sgen_config[k]) for k in parameter_names_to_apply
                                               if k in sgen_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing static generators'
        except RuntimeError:
            return 'Unknown runtime error while parsing static generators'

        # Parse generators
        try:
            # check if dict is empty
            if bool(self.generators):
                for gen_config in self.generators:
                    element_id = gen_config['name']
                    on_bus = self.net_mapping[gen_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_gen(self.net, name=element_id,
                                                                      bus=on_bus, p_mw=0, type=gen_config['type'],
                                                                      max_p_mw=gen_config['max_p_mw'],
                                                                      min_p_mw=gen_config['min_p_mw'],
                                                                      max_q_mvar=gen_config['max_q_mvar'],
                                                                      min_q_mvar=gen_config['min_q_mvar']
                                                                      )

                    # Grab design variable bounds - Note: a controllable element could have multiple
                    # design variables.

                    if element_id in self.controllable_elements:  # check if element is cont/ble
                        controllable_element_dict = self.controllable_elements[element_id]

                        # Iterate through each design variable in the controllable element, grab bounds
                        for variable_name in controllable_element_dict:
                            if variable_name == 'q_mvar':
                                controllable_element_dict[variable_name]['upper'] = \
                                    gen_config['max_q_mvar']
                                controllable_element_dict[variable_name]['lower'] = \
                                    gen_config['min_q_mvar']
                            elif variable_name == 'p_mw':
                                controllable_element_dict[variable_name]['upper'] = \
                                    gen_config['max_p_mw']
                                controllable_element_dict[variable_name]['lower'] = \
                                    gen_config['min_p_mw']
                        #  Put controllable element dict back in central dict
                        self.controllable_elements[element_id] = controllable_element_dict

                    # Apply element parameters
                    parameter_names_to_apply = ['p_mw', 'vm_pu']
                    parameters_to_apply = dict((k, gen_config[k]) for k in parameter_names_to_apply
                                               if k in gen_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing generators'
        except RuntimeError:
            return 'Unknown runtime error while parsing generators'

        # Parse shunt capacitors
        try:
            # check if dict is empty
            if bool(self.shunts):
                for shunt_config in self.shunts:
                    element_id = shunt_config['name']
                    on_bus = self.net_mapping[shunt_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_shunt(self.net, on_bus,
                                                                        name=element_id, q_mvar=0)
                    # Apply element parameters
                    # TODO: make shunt capacitors controllable
                    parameter_names_to_apply = ['q_mvar']
                    parameters_to_apply = dict((k, shunt_config[k]) for k in parameter_names_to_apply
                                               if k in shunt_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing shunt capacitors'
        except RuntimeError:
            return 'Unknown runtime error while parsing shunt capacitors'

        # Parse storage devices
        try:
            # check if dict is empty
            if bool(self.storage):
                for storage_config in self.storage:
                    element_id = storage_config['name']
                    on_bus = self.net_mapping[storage_config['bus']]  # Get local indices from bus mapping
                    self.net_mapping[element_id] = pdpower.create_storage(self.net, on_bus,
                                                                          name=element_id, p_mw=0, max_e_mwh=0)
                    # Apply element parameters
                    parameter_names_to_apply = ['p_mw', 'max_e_mwh', 'q_mvar']
                    parameters_to_apply = dict((k, storage_config[k]) for k in parameter_names_to_apply
                                               if k in storage_config)
                    NetHandle._apply_element_parameters(self, element_id, parameters_to_apply)
        except KeyError:
            return 'One or more keys were not found while parsing storage devices'
        except RuntimeError:
            return 'Unknown runtime error while parsing storage devices'

        # Parse cost functions
        try:
            # check if dict is empty
            if bool(self.cost_functions):
                for costfun_config in self.cost_functions:
                    element_id = costfun_config['name']
                    [element_type, element_global_id] = element_id.split("_")
                    element_type_pdpower = self.pdpower_element_types[element_type]
                    element_local_id = self.net_mapping[element_id]  # Get local index of element.
                    self.cost_coefs[element_id] = pdpower.create_poly_cost(self.net, element_local_id,
                                                                           element_type_pdpower,
                                                                           costfun_config['points'])
        except KeyError:
            return 'One or more keys were not found while parsing cost functions'
        except RuntimeError:
            return 'Unknown runtime error while parsing cost functions'

        # # Parse measurement sensors
        # try:
        # except KeyError:
        #     return 'One or more keys were not found while parsing measurement sensors'
        # except RuntimeError:
        #     return 'Unknown runtime error while parsing measurement sensors'

        return 'Net creation successful'

    def create_net_from_pandapower_library(self, net_name, net_directory):
        """
        This method creates a net from the power system test cases available in pandapower library.
        The net is loaded using standard pandapower methods. Then, the relevant net info is
        extracted in order to fit with the rest of the PSB interface. Namely:
        1. The elements are renamed & mapped as per the nomenclature (eg. bus name '1' becomes 'BUS_1')
        2. The state constraints are extracted and stored in self.state_constraints
        3. The design variable bound constraints, as well as the names of the design variables and
           their type, are stored in self.controllable_elements

        :param net_name: str, must follow pandapower command nomenclature
        :param config: dict {net_name: {'Controllable_Elements': {'ELEMENT_1': {'PARAMETER_1': {'type': ...}}, ...
                                                       'ELEMENT_2': {'PARAMETER_1': {'type': ...}}}} }
        :return: 1. Dict with state constraints
                 2. Dict with design variable bounds
        """
        import pandapower.networks as pn
        # Load specified network
        if net_name not in net_directory:
            return 'net_name not stored in pandapower networks directory'
        net_directory = net_directory[net_name]
        self.controllable_elements = net_directory.get('Controllable_Elements')

        eval_string = 'self.net = pn.{!s}()'.format(net_name)
        exec(eval_string)
        # Map network elements, state constraints, bound constraints
        NetHandle._map_pandapower_net_elements(self)
        NetHandle._map_pandapower_state_constraints(self)
        NetHandle._map_pandapower_bound_constraints(self)
        NetHandle._map_pandapower_cost_functions(self)

    def _map_pandapower_net_elements(self):
        """
        Maps net element names of a standard pandapower network to the PSB nomenclature. Also
        renames net elements using the new nomenclature.
        (dont stress too much over this code - its bad but it does the job)
        """

        for element_prefix in self.pdpower_element_types:
            # Create element mapping
            pdpower_type_name = self.pdpower_element_types[element_prefix]
            element_dataframe = self.net[pdpower_type_name]
            element_names = element_dataframe.name
            pdpower_local_index = element_names.index.tolist()
            element_names = element_names.to_dict()
            inverted_dict = {}
            local_idx_counter = 0
            for key in element_names:
                val = element_names[key]
                # Sometimes, not all elements of default pdpower networks have a defined name.
                # Therefore, force-define one using the local index.
                if val is None:
                    val = pdpower_local_index[local_idx_counter] + 1
                new_key = '{!s}_{!s}'.format(element_prefix, val)
                inverted_dict[new_key] = pdpower_local_index[local_idx_counter]
                local_idx_counter += 1

            # Append to self.net_mapping
            self.net_mapping.update(inverted_dict)

            # Replace old element names with new ones
            for element_name in inverted_dict:
                element_name_new = {'name': element_name}
                NetHandle._apply_element_parameters(self, element_name, element_name_new)

    def _map_pandapower_state_constraints(self):
        """
        Maps net element state constraints of a standard pandapower network to self.state_constraints
        """
        max_loading_percents = self.net.line[['name', 'max_loading_percent']]
        for element_id in max_loading_percents['name'].tolist():
            pdpower_local_idx = self.net_mapping[element_id]
            max = max_loading_percents['max_loading_percent'][pdpower_local_idx]
            self.state_constraints[element_id] = {'line_loading': {'max': max,
                                                                   'min': 0, 'neutral': np.nan}}

        # Map bus state constraints
        bus_state_constraints = self.net.bus[['name', 'max_vm_pu', 'min_vm_pu']]
        for element_id in bus_state_constraints['name'].tolist():
            pdpower_local_idx = self.net_mapping[element_id]
            max = bus_state_constraints['max_vm_pu'][pdpower_local_idx]
            min = bus_state_constraints['min_vm_pu'][pdpower_local_idx]
            self.state_constraints[element_id] = {'bus_voltage': {'max': max,
                                                                  'min': min, 'neutral': np.nan}}

        # Map trafo state constraints
        trafo_state_constraints = self.net.trafo[['name', 'max_loading_percent', 'tap_max',
                                                  'tap_min', 'tap_neutral']]
        for element_id in trafo_state_constraints['name'].tolist():
            pdpower_local_idx = self.net_mapping[element_id]
            max_loading = trafo_state_constraints['max_loading_percent'][pdpower_local_idx]
            max_tap = trafo_state_constraints['tap_max'][pdpower_local_idx]
            min_tap = trafo_state_constraints['tap_min'][pdpower_local_idx]
            tap_neutral = trafo_state_constraints['tap_neutral'][pdpower_local_idx]
            self.state_constraints[element_id] = {'trafo_loading': {'max': max_loading,
                                                                    'min': 0, 'neutral': np.nan},
                                                  'tap': {'max': max_tap,
                                                          'min': min_tap, 'neutral': tap_neutral}}

        # Map external grid state constraints
        ext_grid_state_constraints = self.net.ext_grid[['name', 'max_p_mw', 'min_p_mw',
                                                        'max_q_mvar', 'min_q_mvar']]
        for element_id in ext_grid_state_constraints['name'].tolist():
            pdpower_local_idx = self.net_mapping[element_id]
            max_MW = ext_grid_state_constraints['max_p_mw'][pdpower_local_idx]
            min_MW = ext_grid_state_constraints['min_p_mw'][pdpower_local_idx]
            max_mvar = ext_grid_state_constraints['max_q_mvar'][pdpower_local_idx]
            min_mvar = ext_grid_state_constraints['min_q_mvar'][pdpower_local_idx]
            self.state_constraints[element_id] = {'extgrid_MW': {'max': max_MW,
                                                                 'min': min_MW,
                                                                 'neutral': 1},
                                                  'extgrid_MVar': {'max': max_mvar,
                                                                   'min': min_mvar,
                                                                   'neutral': 1}}

    def _map_pandapower_bound_constraints(self):
        """
        Maps net element bound constraints of a standard pandapower network to self.controllable_elements
        """
        # Map switches
        switch_names = self.net.switch['name']
        for element_id in switch_names.tolist():
            if element_id in self.controllable_elements:
                switch_bounds = self.net.switch['closed']
                controllable_element_dict = self.controllable_elements[element_id]
                controllable_element_dict['closed']['upper'] = 1
                controllable_element_dict['closed']['lower'] = 0

        # Map static generators
        sgen_names = self.net.sgen['name']
        for element_id in sgen_names.tolist():
            if element_id in self.controllable_elements:
                sgen_bounds = self.net.sgen[['max_q_mvar', 'min_q_mvar', 'max_p_mw',
                                             'min_p_mw']]
                controllable_element_dict = self.controllable_elements[element_id]
                pdpower_local_idx = self.net_mapping[element_id]
                for variable_name in self.controllable_elements[element_id]:
                    if variable_name == 'q_mvar':
                        controllable_element_dict[variable_name]['upper'] = \
                            sgen_bounds['max_q_mvar'][pdpower_local_idx]
                        controllable_element_dict[variable_name]['lower'] = \
                            sgen_bounds['min_q_mvar'][pdpower_local_idx]
                    elif variable_name == 'p_mw':
                        controllable_element_dict[variable_name]['upper'] = \
                            sgen_bounds['max_p_mw'][pdpower_local_idx]
                        controllable_element_dict[variable_name]['lower'] = \
                            sgen_bounds['min_p_mw'][pdpower_local_idx]

        # Map generators
        gen_names = self.net.gen['name']
        for element_id in gen_names.tolist():
            if element_id in self.controllable_elements:
                gen_bounds = self.net.gen[['max_q_mvar', 'min_q_mvar', 'max_p_mw',
                                           'min_p_mw']]
                controllable_element_dict = self.controllable_elements[element_id]
                pdpower_local_idx = self.net_mapping[element_id]
                for variable_name in self.controllable_elements[element_id]:
                    if variable_name == 'q_mvar':
                        controllable_element_dict[variable_name]['upper'] = \
                            gen_bounds['max_q_mvar'][pdpower_local_idx]
                        controllable_element_dict[variable_name]['lower'] = \
                            gen_bounds['min_q_mvar'][pdpower_local_idx]
                    elif variable_name == 'p_mw':
                        controllable_element_dict[variable_name]['upper'] = \
                            gen_bounds['max_p_mw'][pdpower_local_idx]
                        controllable_element_dict[variable_name]['lower'] = \
                            gen_bounds['min_p_mw'][pdpower_local_idx]

    def _map_pandapower_cost_functions(self):
        """
        This method extracts data of cost functions from pandapower default nets.
        :return:
        """

        net_has_poly_costs = not self.net.poly_cost.empty
        if net_has_poly_costs:
            for index, rows in self.net.poly_cost:
                element_type_pdpower = rows['et']
                local_pdpower_idx = rows['element']
                element_id = NetHandle._get_element_id_using_idx_and_type(self,
                                                                          element_type_pdpower,
                                                                          local_pdpower_idx)
                cost_dict = {'cp1_eur_per_mw': rows['cp1_eur_per_mw'],
                             'cp2_eur_per_mw2': rows['cp2_eur_per_mw2'],
                             'cq0_eur': rows['cq0_eur'],
                             'cq1_eur_per_mvar': rows['cq1_eur_per_mvar'],
                             'cq2_eur_per_mvar2': rows['cq2_eur_per_mvar2']}
                self.cost_functions[element_id]['points'] = cost_dict
                self.cost_functions[element_id]['cost_type'] = 'poly'

        net_has_pwl_costs = not self.net.pwl_cost.empty
        if net_has_pwl_costs:
            for index, rows in self.net.pwl_cost:
                element_type_pdpower = rows['et']
                local_pdpower_idx = rows['element']
                element_id = NetHandle._get_element_id_using_idx_and_type(self,
                                                                          element_type_pdpower,
                                                                          local_pdpower_idx)
                cost_dict = rows['points']
                self.cost_functions[element_id]['points'] = cost_dict
                self.cost_functions[element_id]['cost_type'] = 'poly'

    def _get_element_id_using_idx_and_type(self, element_type_pdpower, local_idx):

        # Get type of element - find corresponding name in PSB nomenclature
        psb_element_type = None
        for psb_type, pdpower_type in self.pdpower_element_types.items():
            if pdpower_type == element_type_pdpower:
                psb_element_type = psb_type
                break
            else:
                return 'psb_type_not_found'

        # Find all keys containing the PSB element type
        keys_pertaining_to_type = [value for key, value in self.net_mapping.items()
                                   if psb_element_type in key.lower()]
        reduced_mapping_dict = self.net_mapping[keys_pertaining_to_type]

        # For the given local_idx, iterate through keys containing specified element type.
        psb_element_id = None
        for element_id, local_pdpower_idx in reduced_mapping_dict:
            if local_idx == local_pdpower_idx:
                psb_element_id = element_id
                break
            else:
                return 'element_id_not_found'

        return psb_element_id

    def _serialize_bounds_constraints(self, delimiter):
        """
        The functionality of this method is twofold:

        1. It serializes the bounds and constraints so that they can be passed through the
        MATLAB-Python API. The bounds/constraints are separated into two subdictionaries, each
        referring to upper and lower bound (or max min constraint).
        The nomenclature for the state constraint/design variable bound key is
        'element_id/parameter_name'

        2. It saves to .self the serialized keys for each state and design variable;
        this captures the ordering of the aforementioned variables, and it's important information,
        since this ordering must be retained through TCP-IP comms (the MATLAB solver is variable-
        name-agnostic: it only expects that the variables are passed through in the same order
        every time).

        """
        max_state_constraints = {}
        min_state_constraints = {}
        upper_element_bounds = {}
        lower_element_bounds = {}
        design_variable_type = {}

        states_to_apply = self.state_config['which_states']
        idx = 0
        state_keys = []
        for state_to_apply in states_to_apply:
            for element_id in self.state_constraints:
                if state_to_apply in self.state_constraints[element_id]:
                    element_state_constraints = self.state_constraints[element_id]
                    for state_name in element_state_constraints:
                        serialized_key = '{!s}{!s}{!s}'.format(element_id, delimiter, state_name)
                        max_state_constraints[serialized_key] = element_state_constraints[state_name] \
                            .get('max')
                        min_state_constraints[serialized_key] = element_state_constraints[state_name] \
                            .get('min')
                        state_keys.append(serialized_key)
                        idx = idx + 1

        # grab bound constraints
        idx = 0
        design_var_keys = []
        for element_id in self.controllable_elements:
            element_bound_constraints = self.controllable_elements[element_id]
            for parameter_name in element_bound_constraints:
                serialized_key = '{!s}{!s}{!s}'.format(element_id, delimiter, parameter_name)
                upper_element_bounds[serialized_key] = element_bound_constraints[parameter_name] \
                    .get('upper')
                lower_element_bounds[serialized_key] = element_bound_constraints[parameter_name] \
                    .get('lower')
                design_variable_type[serialized_key] = element_bound_constraints[parameter_name] \
                    .get('type')
                design_var_keys.append(serialized_key)
                idx = idx + 1

        state_constraints_serial = {'max': max_state_constraints, 'min': min_state_constraints}
        bound_constraints_serial = {'upper': upper_element_bounds, 'lower': lower_element_bounds}
        serialized_keys = {'states': state_keys, 'design_vars': design_var_keys}
        design_variable_types = design_variable_type

        return state_constraints_serial, bound_constraints_serial, serialized_keys, \
               design_variable_types

    def apply_snapshot_vector(self, snapshot_vector):
        network_variable_dict = NetHandle._deserialize_snapshot_vector(self, snapshot_vector)
        NetHandle.update_net_elements(self, network_variable_dict)

    def _deserialize_snapshot_vector(self, snapshot_vector):
        """
        :param snapshot_vector: series with labels [ELEMENT_1/PARAMETER_1, ELEMENT_1/PARAMETER_2 ... ]
                                and values [param_value_1, param_value_2, ...]
        :return: dict {'ELEMENT_1': {'PARAMETER_1': param_value_1, 'PARAMETER_2': param_value_2},
                       'ELEMENT_2': {'PARAMETER_1': param_value_1, ...}, ...}
        """

        network_variable_dict = {}
        snapshot_vector = snapshot_vector[1:]
        snapshot_vector_dict = snapshot_vector.to_dict()
        for network_variable in snapshot_vector_dict:
            serialized_key_split = network_variable.split('/')
            element_id = serialized_key_split[0]
            parameter_name = serialized_key_split[1]
            if element_id not in network_variable_dict:
                network_variable_dict[element_id] = {}
            network_variable_dict[element_id][parameter_name] = snapshot_vector_dict[network_variable]

        return network_variable_dict

    def return_serialized_bounds_constraints(self):
        delimiter = '_'  # Use _ because fucking matlab doesn't like / as a struct field name...
        return NetHandle._serialize_bounds_constraints(self, delimiter)

    def _return_serialized_ordered_states(self, state_dict):
        """
        Returns the state variables of the network, as calculated in .calculate_specified_states,
        in an ordered dic
        :return: { ELEMENT_1/state_1: value, ELEMENT_2/state_1: value, ... }
        """
        state_serialized_keys = self.serialized_keys['states']
        ordered_state_dict = {}
        for serialized_key in state_serialized_keys:
            serialized_key_list = serialized_key.split('/')
            element_id = serialized_key_list[0]
            state_name = serialized_key_list[1]
            ordered_state_dict[serialized_key] = state_dict[state_name][element_id]

        return ordered_state_dict

    def label_raw_design_vector(self, raw_design_vector):
        """
        Basically maps the raw design vector of the TCP interface back to PSB net_handle notation.
        It uses self.serialized_keys as extracted from .return_serialized_bounds_constraints
        in order to label the raw design vector in the correct order (ie. to assign each design
        variable to the correct network parameter)
        :param raw_design_vector: list [design_var1, design_var2, ...]
        :return labeled_design_vector: dict {'ELEMENT_1': {'parameter_name': design_var1},
                                             'ELEMENT_2: {'parameter_name': design_var2,...}, ...}
        """
        labeled_design_vector = {}
        no_design_vars = len(raw_design_vector)
        design_vector_idx = 0
        design_vars_serialized_keys = self.serialized_keys['design_vars']
        for idx in range(len(raw_design_vector)):

            # Split serialized key, extract element name and parameter name
            design_var_serialized_key = design_vars_serialized_keys[idx]
            design_var_serialized_key = design_var_serialized_key.split('/')
            element_id = design_var_serialized_key[0]
            parameter_name = design_var_serialized_key[1]

            # Make sure ELEMENT_ID subdictionary exists before adding to it.
            if labeled_design_vector.get(element_id) is None:
                labeled_design_vector[element_id] = {}
            labeled_design_vector[element_id][parameter_name] = raw_design_vector[idx]

        return labeled_design_vector

    def update_net_elements(self, update_vector):
        """
        This method applies the update vector to the network. The update vector must retain
        the same nested dict logic as self.controllable_elements. Any parameter that is
        present in the nested dict of an updated-to-be network element will be updated!
        Therefore, make sure that only the desired parameters are included.
        :param update_vector: dict {element1: {parameter1: value, parameter2: value, ...} ...}

        """
        for controllable_element_id in self.controllable_elements:
            controllable_element_dict = self.controllable_elements[controllable_element_id]
            for parameter_name in controllable_element_dict:
                controllable_element_dict[parameter_name] = update_vector[controllable_element_id]. \
                    get(parameter_name)
            NetHandle._apply_element_parameters(self, controllable_element_id,
                                                controllable_element_dict)

    def calculate_specified_kpis(self):
        """
        This method calculates the specified KPIs as given in kpi config.
        :return kpi_results: dict {kpi_name1: res1, ...}
        """
        which_kpis = self.kpi_config['which_kpis']
        which_elements_per_kpi = self.kpi_config['which_elements_per_kpi']
        kpi_results = {}
        # kpi_name must be the same as the method name it refers to (e.g. 'power_losses')
        for kpi_name in which_kpis:
            method_name = '_calculate_{!s}_kpi'.format(kpi_name)
            method_to_call = getattr(NetHandle, method_name)
            if which_elements_per_kpi[kpi_name] == ['all']:
                # Add to this dict whenever new KPIs are added
                element_kpi_mapping = {'power_losses': self.net.line.name,
                                       'voltage_deviation': self.net.bus.name}
                all_element_names = element_kpi_mapping.get(kpi_name)
                which_elements = all_element_names.tolist()
            else:
                which_elements = which_elements_per_kpi[kpi_name]
            kpi_results[kpi_name] = method_to_call(self, which_elements)

        return kpi_results

    def calculate_specified_states(self, return_ordered):
        """
        This method calculates the specified states as given in state config.
        :param return_ordered: If True, returns the state dict in an ordered manner,
                               which is a prerequisite for the matlab optimization interface
        :return states_results:  {state_1: {ELEMENT_1: value, ELEMENT_2: value, ...},
                                  state_2: {ELEMENT_1: value, ELEMENT_2: value, ...},... }
        """
        which_state = self.state_config['which_states']
        which_elements_per_state = self.state_config['which_elements_per_state']
        state_results = {}

        # state_name must be the same exactly as the method name it refers to (e.g. 'line loading')
        for state_name in which_state:
            method_name = '_calculate_{!s}_state'.format(state_name)
            method_to_call = getattr(NetHandle, method_name)
            if which_elements_per_state[state_name] == ['all']:
                # Add to this dict whenever new states are added
                element_state_mapping = {'line_loading': self.net.line.name,
                                         'bus_voltages': self.net.bus.name}
                all_element_names = element_state_mapping.get(state_name)
                which_elements = all_element_names.tolist()
            else:
                which_elements = which_elements_per_state[state_name]
            state_results[state_name] = method_to_call(self, which_elements)

        if return_ordered:
            state_results = NetHandle._return_serialized_ordered_states(self, state_results)
        return state_results

    def _calculate_power_losses_kpi(self, which_lines):
        line_mapping_dict = dict((line_name, self.net_mapping[line_name]) for line_name in which_lines)
        net = self.net
        which_lines_idx = [line_mapping_dict[line_name] for line_name in which_lines]

        res_line_info = net.res_line
        line_info = net.line.loc[which_lines_idx]
        line_resistance = line_info['r_ohm_per_km']
        line_losses = res_line_info['i_from_ka'] ** 2 * line_resistance
        total_losses = sum(line_losses)
        return total_losses

    def _calculate_voltage_deviation_kpi(self, which_buses):
        bus_mapping_dict = dict((bus_name, self.net_mapping[bus_name]) for bus_name in which_buses)
        nominal_voltage = 1
        net = self.net
        which_buses_idx = [bus_mapping_dict[bus_name] for bus_name in which_buses]

        res_bus_info = net.res_bus.loc[which_buses_idx]
        bus_voltage_dev = nominal_voltage - res_bus_info['vm_pu']
        total_voltage_dev = sum(bus_voltage_dev)
        return total_voltage_dev

    # TODO: @ Nikos, we need a _calculate_economic_dispatch_kpi ... of course we must create cost functions first.
    # The coefs of the cost functions of the elements are specified in architecture.yaml and they are numpy arrays
    def _calculate_economic_dispatch_kpi(self, which_generators, which_sgens, which_ext_grid):
        generator_mapping_dict = dict((gen_name, self.net_mapping[gen_name]) for gen_name in which_generators)
        sgen_mapping_dict = dict((sgen_name, self.net_mapping[sgen_name]) for sgen_name in which_sgens)
        # storage_mapping_dict = dict((storage_name, self.net_mapping[storage_name]) for storage_name in which_storage)
        ext_grid_mapping_dict = dict((ext_grid_name, self.net_mapping[ext_grid_name]) for ext_grid_name in which_ext_grid)
        net = self.net
        coefs = self.cost_coefs
        # Create a coefs nested dictionary which contains the coefs per element and per element index
        coefs_dict = {}
        keys = list(coefs.keys())
        values = list(coefs.values())

        split_id = [0] * len(coefs)
        split_idx = [0] * len(coefs)
        i = 0
        while i < len(coefs):
            split_name = keys[i].split("_")
            if len(split_name) > 1:
                split_id[i] = split_name[0]
                split_idx[i] = split_name[1]
            else:
                dumb_name = split_name[0]
                split_name = ['0'] * 2
                split_name[0] = dumb_name
                split_id[i] = split_name[0]
                split_idx[i] = split_name[1]
            i = i + 1

        pos_dict = {}
        for j in range(len(coefs)):
            name = split_id[j]
            for k in range(len(coefs)):
                if split_id[k] == name:
                    pos_dict[split_idx[k]] = values[k]
                    if split_idx[k] == '0':
                        coefs_dict[name] = values[k]
                else:
                    coefs_dict[name] = pos_dict
            pos_dict = {}

        which_gens_idx = [generator_mapping_dict[gen_name] for gen_name in which_generators]
        which_sgens_idx = [sgen_mapping_dict[sgen_name] for sgen_name in which_sgens]
        # which_storage_idx = [storage_mapping_dict[storage_name] for storage_name in which_storage]
        which_ext_grid_idx = [ext_grid_mapping_dict[ext_grid_name] for ext_grid_name in which_ext_grid]

        ## Create individual dictionaries per element
        # Static generators dictionary
        sgens_coefs = {}
        for element_id in coefs_dict:
            if element_id == 'SGEN':
                for i in which_sgens_idx:
                    sgens_coefs[i] = coefs_dict[element_id][i]
        # Generators dictionary
        gens_coefs = {}
        for element_id in coefs_dict:
            if element_id == 'GEN':
                for i in which_gens_idx:
                    gens_coefs[i] = coefs_dict[element_id][i]
        # External grid dictionary
        ext_grid_coefs = {}
        for element_id in coefs_dict:
            if element_id == 'EXT':
                for i in which_ext_grid_idx:
                    ext_grid_coefs[i] = coefs_dict[element_id][i]

        ## Calculate cost for all elements
        # External grid cost calculation
        ext_grid_cost = 0
        for ext_grid_idx in which_ext_grid_idx:
            ext_grid_ap = net.res_ext_grid_idx['p_mw']
            grid_coefs_idx = len(ext_grid_coefs[ext_grid_idx]) - 1
            ext_grid_subcost = 0
            while grid_coefs_idx >= 0:
                ext_grid_subcost = ext_grid_subcost + ext_grid_coefs[ext_grid_idx][grid_coefs_idx] * ext_grid_ap ** (grid_coefs_idx)
                grid_coefs_idx = grid_coefs_idx - 1

            ext_grid_cost = ext_grid_cost + ext_grid_subcost
        # Generators cost calculation
        gens_cost = 0
        for gens_idx in which_gens_idx:
            gen_ap = net.res_gens_idx['p_mw']
            gen_coefs_idx = len(gens_coefs[gens_idx]) - 1
            gen_subcost = 0
            while gen_coefs_idx >= 0:
                gen_subcost = gen_subcost + gens_coefs[gens_idx][gen_coefs_idx] * gen_ap ** (gen_coefs_idx)
                gen_coefs_idx = gen_coefs_idx - 1

            gens_cost = gens_cost + gen_subcost
        # Static generators cost calculation
        sgens_cost = 0
        for sgens_idx in which_sgens_idx:
            sgen_ap = net.res_sgens_idx['p_mw']
            sgen_coefs_idx = len(sgens_coefs[sgens_idx]) - 1
            sgen_subcost = 0
            while sgen_coefs_idx >= 0:
                sgen_subcost = sgen_subcost + sgens_coefs[sgens_idx][sgen_coefs_idx] * sgen_ap ** (sgen_coefs_idx)
                sgen_coefs_idx = sgen_coefs_idx - 1

            sgens_cost = sgens_cost + sgen_subcost

        total_cost = ext_grid_cost + gens_cost + sgens_cost
        return total_cost

    def _calculate_line_loading_state(self, which_lines):
        # TODO: @ Nikos, use this function as a template for the rest of the network states that we agreed upon
        line_mapping = dict((line_name, self.net_mapping[line_name]) for line_name in which_lines)
        net = self.net
        which_lines_idx = [line_mapping[line_name] for line_name in which_lines]

        # Then, grab line loadings using local indices
        line_max_loadings = net.line.loc[which_lines_idx, ['max_loading_percent']]
        line_loadings = net.res_line.loc[which_lines_idx, 'loading_percent']
        line_loadings = line_loadings.tolist()

        line_loading_dict = dict(zip(line_mapping.keys(), line_loadings))
        return line_loading_dict

    def _calculate_bus_voltage_state(self, which_buses):
        bus_mapping = dict((bus_name, self.net_mapping[bus_name]) for bus_name in which_buses)
        net = self.net
        which_buses_idx = [bus_mapping[bus_name] for bus_name in which_buses]

        # Then, grab line loadings using local indices
        bus_voltages = net.res_bus.loc[which_buses_idx, 'vm_pu']
        bus_voltages = bus_voltages.tolist()

        bus_voltages_dict = dict(zip(bus_mapping.keys(), bus_voltages))
        return bus_voltages_dict

    def return_net_mapping(self):
        return_dict = {'net_mapping': self.net_mapping,
                       'controllable_elements': self.controllable_elements}
        return return_dict

    def return_net(self):
        return self.net

    def store_optimization_configs(self, kpi_config, state_config):
        """
        This stores the optimization configs regarding KPIs and states.
        Since the state_config contains the states that are taken into account when
        performing an optimization, the _serialize_bounds_constraints method must be
        called here. Only the states that are involved in optimization are serialized.
        :param kpi_config: see optimization_config.yaml
        :param state_config: see optimization_config.yaml
        """
        self.kpi_config = kpi_config
        self.state_config = state_config
        delimiter = '/'

        # Store in self because they prove useful later, when de-serializing states
        self.state_constraints_serial, self.bound_constraints_serial, self.serialized_keys, \
        self.design_variable_types = NetHandle._serialize_bounds_constraints(self, delimiter)

    def _apply_element_parameters(self, element_name, element_parameters):
        """
        This method updates the parameters of element_name as specified by element_parameters.
        :param element_name: str ('BUS_1', 'LINE_56' etc)
        :param element_parameters: dict {parameter1: value1, parameter2: value2 ...}
        """

        # Get element type in pdpower nomenclature based on element_name
        element_type = re.sub(r'\d+', '', element_name)
        element_type = element_type.replace('_', '')
        element_type_pdpower = self.pdpower_element_types[element_type]
        # Get element index
        element_idx = self.net_mapping[element_name]

        for parameter_name in element_parameters:
            parameter_value = element_parameters[parameter_name]
            if isinstance(parameter_value, str):
                eval_string = "self.net.{!s}.{!s}.at[{!s}] = '{!s}'".format(element_type_pdpower,
                                                                            parameter_name, element_idx,
                                                                            parameter_value)
            else:
                eval_string = 'self.net.{!s}.{!s}.at[{!s}] = {!s}'.format(element_type_pdpower,
                                                                          parameter_name, element_idx, parameter_value)
            exec(eval_string)
