% This objective function represents a weighted sum of KPIs, which are
% passed from the TCP-IP interface. The inequality constraints are handled by adding the
% inequality constraint terms to the returned obj_value.

function obj_value  = obj_fun_dummy(connection, kpi_weight, states_max, states_min, ...
                                             design_vector)

    % Evaluate design vector on network and return KPIs and states
    response_struct = tcpip_interface(connection, design_vector);
    kpi_vector = response_struct{1};
    state_vector = response_struct{2};
    obj_value = kpi_vector(1);
end