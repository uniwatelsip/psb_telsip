% This objective function represents a weighted sum of KPIs, which are
% passed from the TCP-IP interface. The inequality constraints are handled by adding the
% inequality constraint terms to the returned obj_value.

function obj_value  = obj_fun_1(connection, kpi_weight, states_max, states_min, design_vector)

    % Evaluate design vector on network and return KPIs and states
    response_struct = tcpip_interface(connection, design_vector);
    kpi_vector = response_struct{1};
    state_vector = response_struct{2};

    % Calculate objective
    objective = kpi_vector*kpi_weight;

    % Find violated states
    upper_violated_states = state_vector > states_max;
    lower_violated_states = state_vector < states_min;
    penalty_term = 0;
    if any(upper_violated_states)
        upper_violated_states_distance = abs(state_vector(upper_violated_states) - ...
                                             states_max(upper_violated_states));
        upper_penalty_term = exp(5*upper_violated_states_distance);
        penalty_term = penalty_term + upper_penalty_term;
    end
    if any(lower_violated_states)
        lower_violated_states_distance = abs(state_vector(lower_violated_states) - ...
                                             states_min(lower_violated_states));
        lower_penalty_term = exp(5*lower_violated_states_distance);
        penalty_term = penalty_term + lower_penalty_term;
    end

    % TODO: Add penalty weights vector (which would substitute the '5' scalar in the exp() )
    % -----------------------------------------------------------------
    obj_value = objective + penalty_term;
end