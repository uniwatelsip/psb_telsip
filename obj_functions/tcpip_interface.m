% TCPIP Interface that utilizes UUID per packet, and can handle multiple VECTOR messages
% per packet. The delimiter between messages is '/'. The output is a single vector or a cell array containing
% multiple vectors.
% --------------------------
% CONSIDERATIONS:
% --------------------------
% 1)One should not expect that the significant digit precision to be maintained
%   when data is passed through the interface. In practical terms, this means
%   rounding to 7-8 decimals or less.
% 2) There is a maximum number of design variables that can be utilized for a given
%    buffer size. Make sure that this is correctly set.


function output = tcpip_interface(connection, design_vector)

    java_uuid =  java.util.UUID.randomUUID;
    uuid_request = char(java_uuid.toString);
    design_vector_str = mat2str(design_vector);
    msg = strcat(uuid_request, design_vector_str);
    tcp_message = [msg newline];
    fwrite(connection, tcp_message);

    % Response read loop. Handle UUID
    valid_uuid_found = false;
    empty_responses = 0;
    empty_responses_threshold = 100;
    empty_resp_threshold_reached = false;
    wrong_uuid = 0;
    wrong_uuid_threshold = 30;   % This should be the number of total requests that the caller will use
    while not(valid_uuid_found)

        response = fread(connection, [1, connection.BytesAvailable]);
        response_str = native2unicode(response, 'UTF-8');

        % Use a try-catch here because the response could be empty, thus
        % breaking on response_str(1:36)
        try
            uuid_response = response_str(1:36);
            % check that the returned UUID is the same as the expected one
            if strcmp(uuid_response, uuid_request)
                valid_uuid_found = true;
            else
                wrong_uuid = wrong_uuid+1;
                % check that we havent reached max wrong UUIDs
                if wrong_uuid > wrong_uuid_threshold
                    ME = MException('TCPInterface:ThresholdUUID', 'Reached threshold number of wrong UUIDs');
                    throw(ME)
                end
            end
        catch
            empty_responses = empty_responses+1;
            % check that we havent reached max empty responses
            if empty_responses > empty_responses_threshold
                ME = MException('TCPInterface:ThresholdEmptyResponses', 'Reached threshold number of empty responses');
                throw(ME)
            end
        end
    end

    % Parse the message
    delimiter = '/';
    message_str = response_str(37:end);             % Split from UUID
    message_str = erase(message_str, ',');          % Ditch commas
    contains_multiple_messages = contains(message_str, delimiter);
    if contains_multiple_messages
        message_str_array = split(message_str, '/');     % Split individual messages
        message_cell_array = {};
        for message_idx = 1:size(message_str_array, 1)
            message_cell_array{message_idx} = str2num(message_str_array{message_idx});
        end
        output = message_cell_array;
    else
        output = str2num(message_str);
    end
end
