import pandapower as pdpower
import pandas as pd
import random

def load_simbench_data(path):
    RESProfile = pd.read_csv('simbench/{!s}/RESP'.format(path))
    LoadProfile = pd.read_csv('simbench/{!s}/LoadProfile'.format(path))

def duplicate_dummy_data():
    dummy_timeseries = pd.read_csv('dummy_timeseries.csv')
    extra_load_labels = ['LOAD_4/p_mw', 'LOAD_4/q_mvar', 'LOAD_5/p_mw', 'LOAD_5/q_mvar', 'LOAD_6/p_mw', 'LOAD_6/q_mvar',
                    'LOAD_7/p_mw', 'LOAD_7/q_mvar', 'LOAD_8/p_mw', 'LOAD_8/q_mvar', 'LOAD_9/p_mw', 'LOAD_9/q_mvar',
                    'LOAD_10/p_mw', 'LOAD_10/q_mvar', 'LOAD_11/p_mw', 'LOAD_11/q_mvar', 'LOAD_12/p_mw', 'LOAD_12/q_mvar',
                    'LOAD_13/p_mw', 'LOAD_13/q_mvar', 'LOAD_14/p_mw', 'LOAD_14/q_mvar', 'LOAD_15/p_mw', 'LOAD_15/q_mvar']
    extra_sgen_labels = ['SGEN_4/p_mw', 'SGEN_5/p_mw', 'SGEN_6/p_mw',  'SGEN_7/p_mw', 'SGEN_8/p_mw', 'SGEN_9/p_mw',
                    'SGEN_10/p_mw', 'SGEN_11/p_mw']
    for label in extra_load_labels:
        if 'q_mvar' in label:
            column_to_append = 'LOAD_2/q_mvar'
        else:
            column_to_append = 'LOAD_2/p_mw'
        dummy_timeseries[label] = dummy_timeseries[column_to_append].apply(lambda x: x+random.random())

    for label in extra_sgen_labels:
        if 'q_mvar' in label:
            column_to_append = 'SGEN_2/q_mvar'
        else:
            column_to_append = 'SGEN_2/p_mw'
        dummy_timeseries[label] = dummy_timeseries[column_to_append].apply(lambda x: x+random.random())

        dummy_timeseries.to_csv('dummy_timeseries_2.csv')

if __name__ == "__main__":
    import yaml
    import pathlib
    duplicate_dummy_data()
