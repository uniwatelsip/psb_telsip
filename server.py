"""
:author: Myron Papadimitrakis (TELSIP, University of West Attica)
:date: 11/09/20
"""

from flask import Flask, request, make_response, jsonify
import pandas as pd
from main import TelsipPSB
import matlab.engine
from datetime import datetime
import logging
import threading
import warnings

app = Flask(__name__)
mateng = matlab.engine.start_matlab()              # Start matlab engine
lock = threading.Lock()

# Create logger
server_logger = logging.getLogger('server_logger')
server_timestamp = datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
filename_string = 'serverlog.log'
# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('server.log')
c_handler.setLevel(logging.INFO)
f_handler.setLevel(logging.WARNING)
# Create formatters and add it to handlers
c_format = logging.Formatter('%(asctime)s - %(message)s')
f_format = logging.Formatter('%(asctime)s - %(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)
# Add handlers to the logger
server_logger.addHandler(c_handler)
server_logger.addHandler(f_handler)
# log server timestamp start
server_logger.warning('Started Server @ {!s}'.format(server_timestamp))
# Ignore warnings (specific server messages still log)
warnings.filterwarnings("ignore")

@app.route("/is_online")
def is_online():
    """
    Checks if the server is online.
    :returns Server is online
    :rtype python dict
    """
    return "Server is Online", 200


@app.route("/run_optimization", methods=['POST'])
def run_optimization():
    """
    Receives a post request containing all relevant data so that telsip-PSB can
    1. Create a net object based on architecture and apply the snapshot vector
    2. Run an optimization based on optimization_config
    3. Return the setpoints, along with some critical network info
    :arg: http post request containing a jsonified dict with the following data:
             {'architecture': architecture, 'optimization_config': optimization_config,
             'snapshot_vector': snapshot_vector}
             The snapshot_vector is a pd.Series converted to dict (df.to_dict)
    :returns: dict {'setpoints': setpoint dict, 'state_variables': state_variable_dict,
                   'info': info_dict}
    :rtype: string
    """
    post_data = request.get_json()

    # Turn snapshot vector into series
    snapshot_vector = pd.DataFrame.from_dict(post_data.get('snapshot_vector'),
                                             orient='columns')
    post_data['snapshot_vector'] = snapshot_vector.squeeze()    # Squeeze to Series

    # Initialize TelsipPSB
    telsip_psb = TelsipPSB(post_data, mateng)
    opf_results = telsip_psb.run_opf()

    return_msg = {'optimization_results': opf_results}
    return return_msg, 200

@app.route("/run_control", methods=['POST'])
def run_control():
    """
    Receives a post request containing all relevant data so that telsip-PSB can
    1. Create a net object based on architecture
    2. Run a control simulation based on optimization_config and timeseries
    3. Return the setpoints for each snapshot datetime, along with some critical network info
    :arg: http post request containing a jsonified dict with the following data:
             {'architecture': architecture, 'optimization_config': optimization_config,
              'timeseries': timeseries}
             The timeseries is a pd.Dataframe converted to dict (df.to_dict)
    :returns: dict {'setpoints': setpoint dict, 'state_variables': state_variable_dict,
                    'info': info_dict}
    :rtype: string
    """
    post_data = request.get_json()

    # Turn snapshot vector into series
    post_data['timeseries'] = pd.DataFrame.from_dict(post_data.get('timeseries'), orient='columns')

    # Initialize TelsipPSB
    telsip_psb = TelsipPSB(post_data, mateng)
    opf_results = telsip_psb.run_control()

    return_msg = {'control_results': opf_results}
    return return_msg, 200

@app.route("/algorithm_catalog")
def algorithm_catalog():
    """
    :returns: A list of available algorithms
    :rtype: dict
    """
    telsip_psb = TelsipPSB({}, {})
    catalog = telsip_psb.return_catalog()
    return catalog

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': "https://media.giphy.com/media/hEc4k5pN17GZq/giphy.gif"}), 404)

if __name__ == "__main__":
    app.run(debug=False, threaded=True, host="localhost", port=8081)

