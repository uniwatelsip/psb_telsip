from socket import *
import ast

class TCPServer:
    def __init__(self, server_config, power_flow_handle, optimization_self):
        self.host = server_config['host']
        self.port = server_config['port']
        self.socket_config = server_config
        self.backlog = server_config['backlog']
        self.run_power_flow = power_flow_handle
        self.optimization_self = optimization_self
        # TODO: ask Vangelis whether this is good practice:
        self.net_handle = optimization_self.net_handle

    def launch_tcp_server(self):

        # Initialize server
        self.Socket = TCPInterface()
        self.Socket.bind(('localhost', self.port))
        self.Socket.listen(self.backlog)
        while True:
            c, a = self.Socket.accept()
            print(f'server: {a[0]}:{a[1]} connected')
            while True:
                design_vector = c.get_msg()
                if not design_vector: break
                # Apply design vector
                labelled_design_vector = self.net_handle.label_raw_design_vector(design_vector)
                self.net_handle.update_net_elements(labelled_design_vector)
                self.run_power_flow(self.optimization_self)
                kpis = self.net_handle.calculate_specified_kpis()
                states = self.net_handle.calculate_specified_states(True)
                c.put_multiple_msg([[*kpis.values()], [*states.values()]])
            print(f'server: {a[0]}:{a[1]} disconnected')
            c.close()
            break


    def _terminate_tcp_server(self):
        self.Socket.close()

class TCPInterface:

    '''
    TCP-IP Interface
    Creates a TCP-IP interface and maintains a socket connection with the matlab client. It handles all
    tcp-ip communications.
    '''

    def __init__(self, s=None):
        self.sock = socket() if s is None else s
        self.buffer = b''
        self.uuid = ''
        self.total_packets = 0
        self.buffer_size = 10000
        self.delimiter = '/'

    def connect(self, addr):
        self.sock.connect(addr)
        self.client_address = addr

    def bind(self, addr):
        self.client_address = addr
        self.sock.bind(addr)

    def listen(self, n):
        self.sock.listen(n)

    def accept(self):
        client, a = self.sock.accept()
        # Wrap the client socket in a Socket.
        return TCPInterface(client), a

    def get_msg(self):
        # Buffer data until a newline is found.
        while b'\n' not in self.buffer:
            data = self.sock.recv(self.buffer_size)
            if not data:
                return b''
            self.buffer += data
        # split off the message bytes from the buffer.
        msg,_,self.buffer = self.buffer.partition(b'\n')
        msg_string = msg.decode('utf-8')
        self.uuid = msg_string[:36]
        actual_msg = msg_string[36:]
        self.total_packets = self.total_packets + 1
        actual_msg = actual_msg.replace(' ', ', ')
        actual_msg = ast.literal_eval(actual_msg)
        return actual_msg

    def put_msg(self, msg):
        # Add UUID
        uuid_msg = self.uuid + str(msg)
        response = f'{uuid_msg}'
        self.sock.sendall(response.encode('utf-8') + b'\n')

    def put_multiple_msg(self, msg_list):
        concat_msg = str(msg_list.pop(0))
        for msg in msg_list:
            concat_msg = concat_msg + self.delimiter + str(msg)
        uuid_msg = self.uuid + concat_msg
        response = f'{uuid_msg}'
        self.sock.sendall(response.encode('utf-8') + b'\n')

    def close(self):
        print('Total packets received: {!s}'.format(self.total_packets))
        self.sock.close()
